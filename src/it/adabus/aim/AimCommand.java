package it.adabus.aim;

public class AimCommand {
    private String messageType;
    private String cmdId;
    private String action;
    private Target target;

    public AimCommand(String messageType, String cmdId, String action) {
        this(messageType,cmdId,action,null,null);
    }

    public AimCommand(String messageType, String cmdId, String action, String targetType, String targetValue) {
        this.messageType = messageType;
        this.cmdId = cmdId;
        this.action = action;
        this.target = new Target(targetType, targetValue);
    }

    public String getMessageType() {
        return messageType;
    }

    public String getCmdId() {
        return cmdId;
    }

    public String getAction() {
        return action;
    }

    public String getTargetType() {
        return target.getType();
    }

    public String getTargetValue() {
        return target.getValue();
    }

    class Target {
        private String type;
        private String value;

        public Target(String type, String value) {
            this.type = type;
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
