package it.adabus.aim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogReaderThread implements Runnable {


    private boolean running;
    private long updateInterval;
    private String fileName;
    private Logger readThreadLog = LogManager.getLogger(LogReaderThread.class);
    private Map<String, AimCommand> messageQueue;

    public LogReaderThread(long updateInterval, String fileName, String timeUnit) {
        if(timeUnit=="s"){
            updateInterval *= 1000;
        }
        this.updateInterval = updateInterval;
        this.fileName = fileName;
        this.running = true;
        messageQueue = new HashMap<>();

    }

    @Override
    public void run() {
        File file = new File(fileName);
        long filePointer = 0;
        try {
            readThreadLog.info("Start reading log file: "+ fileName+"...");
            while (running) {
                long len = file.length();
                if (len < filePointer) {
                    readThreadLog.warn("Log file was reset. Restarting logging from start of file.");
                    filePointer = len;
                }
                else if (len > filePointer) {
                    // File must have had something added to it!
                    RandomAccessFile raf = new RandomAccessFile(file, "r");
                    raf.seek(filePointer);
                    String line;
                    while ((line = raf.readLine()) != null) {
                        String message = searchMessage(line);
                        if(message != null){
                            manageQueue(unmarshalMessage(message));

                        }
                    }
                    filePointer = raf.getFilePointer();
                    raf.close();
                }
                Thread.sleep(updateInterval);
            }
        }
        catch (Exception e) {
            readThreadLog.error("Fatal error reading log file, log tailing has stopped.",e);
        }
    }

    private void manageQueue(AimCommand aimCommand) {
        if(aimCommand.getMessageType().equals("request")){
            messageQueue.put(aimCommand.getCmdId(),aimCommand);
        }else if(aimCommand.getMessageType().equals("answer")){
            if(messageQueue.size()==0){
                readThreadLog.warn("Remove command without request! CmdId: "+ aimCommand.getCmdId());
            }
            messageQueue.remove(aimCommand.getCmdId());
        }
    }

    private AimCommand unmarshalMessage(String message) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        AimCommand aimCommand = null;
        try {
            builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(message)));
            Element xmlMessage = ((Element) document.getElementsByTagName("message").item(0));
            String cmdid = xmlMessage.getAttribute("cmdid");
            String type = xmlMessage.getAttribute("type");

            Element xmlAction = ((Element) document.getElementsByTagName("action").item(0));
            String action = xmlAction.getFirstChild().getTextContent();

            Element xmlTarget = ((Element) document.getElementsByTagName("target").item(0));
            if(xmlTarget != null){
                String targetType = xmlTarget.getAttribute("type");
                String targetValue = xmlTarget.getFirstChild().getTextContent();
                aimCommand = new AimCommand(type,cmdid,action,targetType,targetValue);
            }else{
                aimCommand = new AimCommand(type,cmdid,action);
            }

        } catch (Exception e) {
            readThreadLog.error("Error unmarshalling message...",e);
        }

        return aimCommand;
    }

    private String searchMessage(String line) {
        Pattern p = Pattern.compile("<message.*message>");
        Matcher m = p.matcher(line);
        return m.find() ? m.group() : null;
    }

    public void stopThread(){
        this.running = false;
    }

    public Map<String, AimCommand> getMessageQueue() {
        return messageQueue;
    }
}
