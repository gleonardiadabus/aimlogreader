package it.adabus.aim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AimLogReader {
    private final static Logger log = LogManager.getRootLogger();

    public static void main(String[] args) throws InterruptedException {
        int statusQueue = 0;
        Thread.currentThread().setName("QueueReader");
        long updateInterval = 10;
        String fileName = "/home/gleonardi/Documents/testlog.txt";
        String timeUnit = "m";
        if (args.length >= 1) {
            try {
                updateInterval = Long.parseLong(args[0]);
            } catch (NumberFormatException e) {
                log.error("Invalid update interval!");
                return;
            }
        }
        if(args.length>=2){
            fileName= args[1];
        }
        if(args.length>=3){
            timeUnit = args[2];
        }
        LogReaderThread logReader = new LogReaderThread(updateInterval, fileName,timeUnit);
        Thread thread = new Thread(logReader);
        thread.setName("LogReaderThread");
        thread.start();

        Thread inputThread = new Thread(new InputReaderThread(logReader.getMessageQueue()));
        inputThread.setName("InputReaderThread");
        inputThread.start();

        while (true) {
            Thread.sleep(2000);
            int queueSize = logReader.getMessageQueue().size();
            if(statusQueue != queueSize){
                log.warn("Status queue: "+ queueSize);
                statusQueue = queueSize;
            }
        }
    }
}
