package it.adabus.aim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Scanner;

public class InputReaderThread implements Runnable {

    private Logger inputReadLog = LogManager.getLogger(InputReaderThread.class);
    private Map<String, AimCommand> messageQueue;

    public InputReaderThread(Map messageQueue) {
        this.messageQueue = messageQueue;

    }

    @Override
    public void run() {
        try {
            inputReadLog.info("Press q to show message queue status: ");
            while (true) {
                Scanner scanner = new Scanner(System.in);
                if (scanner.nextLine().equals("q")) {
                    String result = "\nQueue status: \n";
                    result += " Queue size: " + messageQueue.size();
                    if(messageQueue.size()>0) {
                        result += " Commands in queue: ";
                        for (Map.Entry entry : messageQueue.entrySet()) {
                            result +="      -Command id: " + entry.getKey();
                        }
                    }
                    inputReadLog.info(result);
                }
                Thread.sleep(5000);
            }
        } catch (Exception e) {
            inputReadLog.error("Fatal error.", e);
        }
    }


}
